




window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // add the d-none class to the loading icon
      const spinner = document.getElementById('spinner');
      spinner.classList.add("d-none");
      // remove the d-none class from the select tag
      const dropdown = document.getElementById('conference');
      dropdown.classList.remove("d-none");

      const formTag = document.getElementById('create-attendee-form');
      formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(Object.fromEntries(formData));
        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            const attendeeForm = document.getElementById('create-attendee-form');
            attendeeForm.classList.add('d-none');
            const succeed = document.getElementById('success-message');
            succeed.classList.remove('d-none');
        }
      });
    }
  });