function createCard(name, description, pictureUrl, start_date, end_date) {
    return `
    <div class="col">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
          <ul class="list-group list-group-flush">
          <li class="list-group-item">${start_date} - ${end_date}</li>
        </div>
      </div>
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    
    const url = 'http://localhost:8000/api/conferences/';
    
    try {
        const response = await fetch(url);
        
        if (!response.ok) {
        // Figure out what to do when the response is bad
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const html = createCard(name, description, pictureUrl, starts, ends);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }
            }
        }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }
  
  });